//
//  Category.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import Foundation

struct Category: Decodable {
    let id: Int
    let name: String
}
