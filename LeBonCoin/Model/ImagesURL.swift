//
//  ImagesURL.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import Foundation

struct ImagesURL: Decodable {
    let small: URL?
    let thumb: URL?
}
