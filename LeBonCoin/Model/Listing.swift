//
//  Listing.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import Foundation

struct Listing: Decodable {
    let id: Int
    let categoryId: Int
    let title: String
    let description: String
    let price: Double
    let imagesURL: ImagesURL
    let creationDate: Date
    let isUrgent: Bool

    enum CodingKeys: String, CodingKey {
        case id
        case categoryId = "category_id"
        case title
        case description
        case price
        case imagesURL = "images_url"
        case creationDate = "creation_date"
        case isUrgent = "is_urgent"
    }
}

extension Listing {
    func displayablePrice(locale: Locale = Locale(identifier: "fr_FR")) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = locale

        return numberFormatter.string(from: NSNumber(value: price)) ?? Localizables.undefinedPrice
    }

    var urgent: String? {
        return isUrgent ? Localizables.urgent : nil
    }
}
