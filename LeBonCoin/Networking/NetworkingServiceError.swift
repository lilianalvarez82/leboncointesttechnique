//
//  NetworkingServiceError.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import Foundation

enum NetworkingServiceError: Error {
    case internetConnectivity
    case noData
    case decode
    case invalidUrl
}

