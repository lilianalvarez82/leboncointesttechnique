//
//  ItemsService.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import Foundation

typealias CompletionHandler<T> = (Result<T, NetworkingServiceError>) -> Void

protocol ItemsServiceProtocol {
    func getListings(completion: @escaping (CompletionHandler<[Listing]>))
    func getCategories(completion: @escaping (CompletionHandler<[Category]>))
}

final class ItemsService {
    static let shared: ItemsServiceProtocol = ItemsService()
    private let urlSession: URLSession
    private let baseURLString = "https://raw.githubusercontent.com/leboncoin/paperclip/master"
    private let dateJSONDecoder: DateJSONDecoder

    init(
        urlSession: URLSession = .shared,
        dateJSONDecoder: DateJSONDecoder = .init()
    ) {
        self.urlSession = urlSession
        self.dateJSONDecoder = dateJSONDecoder
    }
}

extension ItemsService: ItemsServiceProtocol {
    /// MARK : - Enable query listing of objects.
    func getListings(completion: @escaping CompletionHandler<[Listing]>) {
        guard let listingURL = URL(string: "\(baseURLString)/listing.json") else {
            completion(.failure(.invalidUrl))
            return
        }

        urlSession.dataTask(with: listingURL) { [weak self] data, _, error in
            guard error == nil, let me = self else {
                completion(.failure(.internetConnectivity))
                return
            }

            guard let data = data else {
                completion(.failure(.noData))
                return
            }

            do {
                let itemList = try me.dateJSONDecoder.decode([Listing].self, from: data)
                completion(.success(itemList))
            } catch {
                completion(.failure(.decode))
            }
        }.resume()
    }

    /// MARK : - Enable query catogories of listing.
    func getCategories(completion: @escaping CompletionHandler<[Category]>) {
        guard let listingURL = URL(string: "\(baseURLString)/categories.json") else {
            completion(.failure(.invalidUrl))
            return
        }

        urlSession.dataTask(with: listingURL) { data, _, error in
            guard error == nil else {
                completion(.failure(.internetConnectivity))
                return
            }

            guard let data = data else {
                completion(.failure(.noData))
                return
            }

            do {
                let itemList = try JSONDecoder().decode([Category].self, from: data)
                completion(.success(itemList))
            } catch {
                completion(.failure(.decode))
            }
        }.resume()
    }
}
