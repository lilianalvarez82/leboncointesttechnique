//
//  ImageDownloaderService.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import UIKit

protocol ImageDownloaderServiceProtocol {
    func downloadImage(url: URL, completion: @escaping (CompletionHandler<UIImage>))
}

typealias ImageCache = NSCache<NSString, UIImage>

final class ImageDownloaderService {
    static let shared: ImageDownloaderServiceProtocol = ImageDownloaderService()
    private let imageCache: ImageCache
    private let urlSession: URLSession

    init(
        imageCache: ImageCache = .init(),
        urlSession: URLSession = URLSession.shared
    ) {
        self.imageCache = imageCache
        self.urlSession = urlSession
    }
}

extension ImageDownloaderService: ImageDownloaderServiceProtocol {
    func downloadImage(url: URL, completion: @escaping CompletionHandler<UIImage>) {
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            completion(.success(cachedImage))
        } else {
            urlSession.dataTask(with: url) { [weak self] data, _, error in
                guard error == nil, let me = self else {
                    completion(.failure(.internetConnectivity))
                    return
                }

                guard let data = data else {
                    completion(.failure(.noData))
                    return
                }

                guard let image = UIImage(data: data) else {
                    completion(.failure(.decode))
                    return
                }

                me.imageCache.setObject(image, forKey: url.absoluteString as NSString)
                completion(.success(image))
            }.resume()
        }
    }
}
