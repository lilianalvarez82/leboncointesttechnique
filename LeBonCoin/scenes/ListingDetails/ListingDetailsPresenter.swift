//
//  ListingDetailsPresenter.swift
//  LeBonCoin
//
//  Created by Lilian on 12/02/2021.
//

import UIKit

protocol ListingDetailsView: AnyObject {
    func reload(image: UIImage)
    func reload(listing: Listing, category: Category?)
}

final class ListingDetailsPresenter {
    private let listing: Listing
    private let category: Category?
    private let imageDownloaderService: ImageDownloaderServiceProtocol
    unowned private let view: ListingDetailsView

    init(
        listing: Listing,
        category: Category?,
        imageDownloaderService: ImageDownloaderServiceProtocol = ImageDownloaderService.shared,
        view: ListingDetailsView
    ) {
        self.listing = listing
        self.category = category
        self.imageDownloaderService = imageDownloaderService
        self.view = view
    }
}

extension ListingDetailsPresenter {
    func refresh() {
        view.reload(listing: listing, category: category)

        guard let thumbURL = listing.imagesURL.thumb else { return }
        imageDownloaderService.downloadImage(url: thumbURL) { [weak self] (result) in
            guard let me = self else { return }

            switch result {
            case .success(let image):
                me.view.reload(image: image)
            case .failure(let error):
                print("Error when fetching thumb listing image = \(error.localizedDescription)")
            }
        }
    }
}
