//
//  ListingDetailsViewController.swift
//  LeBonCoin
//
//  Created by Lilian on 12/02/2021.
//

import UIKit

final class ListingDetailsViewController: UIViewController {
    private let scrollView = UIScrollView()
    private let imageView = UIImageView()
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    private let priceLabel = UILabel()
    private let dateLabel = UILabel()
    private let categoryLabel = UILabel()
    private let urgentLabel = UILabel()
    var presenter: ListingDetailsPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        configureConstraints()
        presenter.refresh()
    }
}

private extension ListingDetailsViewController {
    func configureView() {
        scrollView.showsVerticalScrollIndicator = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = .white

        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "placeholder")

        titleLabel.font = .systemFont(ofSize: 16, weight: .bold)
        titleLabel.numberOfLines = 0
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        descriptionLabel.font = .systemFont(ofSize: 14, weight: .regular)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false

        priceLabel.textColor = .orange
        priceLabel.font = .systemFont(ofSize: 15, weight: .regular)
        priceLabel.translatesAutoresizingMaskIntoConstraints = false

        dateLabel.font = .systemFont(ofSize: 14)
        dateLabel.translatesAutoresizingMaskIntoConstraints = false

        categoryLabel.textColor = .lightGray
        categoryLabel.font = .systemFont(ofSize: 14, weight: .light)
        categoryLabel.translatesAutoresizingMaskIntoConstraints = false

        urgentLabel.font = .systemFont(ofSize: 14, weight: .semibold)
        urgentLabel.textColor = .red
        urgentLabel.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(scrollView)
        scrollView.addSubview(imageView)
        scrollView.addSubview(titleLabel)
        scrollView.addSubview(descriptionLabel)
        scrollView.addSubview(priceLabel)
        scrollView.addSubview(dateLabel)
        scrollView.addSubview(urgentLabel)
        scrollView.addSubview(categoryLabel)
    }

    func configureConstraints() {
        let safeArea = view.safeAreaLayoutGuide
        let margin: CGFloat = 16

        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            scrollView.topAnchor.constraint(equalTo: safeArea.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])

        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            imageView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            imageView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            imageView.heightAnchor.constraint(equalToConstant: view.frame.width),

            titleLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: margin),
            titleLabel.trailingAnchor.constraint(equalTo: urgentLabel.trailingAnchor, constant: -margin),
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: margin),

            priceLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: margin),
            priceLabel.trailingAnchor.constraint(equalTo: urgentLabel.trailingAnchor, constant: -margin),
            priceLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: margin),

            urgentLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -margin),
            urgentLabel.centerYAnchor.constraint(equalTo: priceLabel.centerYAnchor),

            dateLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: margin),
            dateLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -margin),
            dateLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 8),

            categoryLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: margin),
            categoryLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -margin),
            categoryLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 8),

            descriptionLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: margin),
            descriptionLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -margin),
            descriptionLabel.topAnchor.constraint(equalTo: categoryLabel.bottomAnchor, constant: margin),
            descriptionLabel.bottomAnchor.constraint(greaterThanOrEqualTo: scrollView.bottomAnchor, constant: -margin),
        ])
    }
}

extension ListingDetailsViewController: ListingDetailsView {
    func reload(image: UIImage) {
        DispatchQueue.main.async {
            self.imageView.image = image
        }
    }

    func reload(listing: Listing, category: Category?) {
        urgentLabel.text = listing.urgent
        categoryLabel.text = category?.name
        titleLabel.text = listing.title
        descriptionLabel.text = listing.description
        dateLabel.text = listing.creationDate.toDisplayableString()
        priceLabel.text = listing.displayablePrice()
    }
}
