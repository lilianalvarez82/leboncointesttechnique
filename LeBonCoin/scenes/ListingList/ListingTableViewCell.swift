//
//  ListingTableViewCell.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import UIKit

final class ListingTableViewCell: UITableViewCell {
    private var presenter: ListingCellPresenter?
    private let thumbImageView = UIImageView()
    private let titleLabel = UILabel()
    private let priceLabel = UILabel()
    private let urgencyLabel = UILabel()
    private let categoryLabel = UILabel()

    func configure(listing: Listing, category: Category?) {
        configureView()
        configureConstraint()

        presenter = ListingCellPresenter(listing: listing, category: category, view: self)
    }

    func refreshData() {
        presenter?.refresh()
    }
}

extension ListingTableViewCell: ListingCellView {
    func reload(image: UIImage) {
        DispatchQueue.main.async {
            self.thumbImageView.image = image
        }
    }

    func reload(listing: Listing, category: Category?) {
        titleLabel.text = listing.title
        priceLabel.text = listing.displayablePrice()
        urgencyLabel.text = listing.urgent
        urgencyLabel.isHidden = !listing.isUrgent
        categoryLabel.text = category?.name
    }
}

private extension ListingTableViewCell {
    func configureView() {
        thumbImageView.contentMode = .scaleAspectFit
        thumbImageView.image = UIImage(named: "placeholder")
        thumbImageView.translatesAutoresizingMaskIntoConstraints = false

        titleLabel.font = .systemFont(ofSize: 15, weight: .medium)
        titleLabel.numberOfLines = 2
        titleLabel.textAlignment = .left
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        priceLabel.font = .systemFont(ofSize: 16, weight: .bold)
        priceLabel.textColor = .orange
        priceLabel.textAlignment = .left
        priceLabel.translatesAutoresizingMaskIntoConstraints = false

        urgencyLabel.font = .italicSystemFont(ofSize: 12)
        urgencyLabel.textColor = .red
        urgencyLabel.textAlignment = .right
        urgencyLabel.translatesAutoresizingMaskIntoConstraints = false

        categoryLabel.font = .systemFont(ofSize: 12, weight: .light)
        categoryLabel.textColor = .lightGray
        categoryLabel.textAlignment = .left
        categoryLabel.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(thumbImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(categoryLabel)
        contentView.addSubview(urgencyLabel)
    }

    func configureConstraint() {
        let margin: CGFloat = 16

        NSLayoutConstraint.activate([
            thumbImageView.widthAnchor.constraint(equalToConstant: 120),
            thumbImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            thumbImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            thumbImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: margin),

            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: margin),
            titleLabel.leadingAnchor.constraint(equalTo: thumbImageView.trailingAnchor, constant: margin),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -margin),

            priceLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            priceLabel.leadingAnchor.constraint(equalTo: thumbImageView.trailingAnchor, constant: margin),
            priceLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -margin),

            urgencyLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -margin),
            urgencyLabel.leadingAnchor.constraint(equalTo: thumbImageView.trailingAnchor, constant: margin),
            urgencyLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -margin),

            categoryLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -margin),
            categoryLabel.leadingAnchor.constraint(equalTo: thumbImageView.trailingAnchor, constant: margin),
            categoryLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -margin),
        ])
    }
}
