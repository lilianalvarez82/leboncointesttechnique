//
//  ListingPresenter.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import Foundation

protocol ListingPresenterView: AnyObject {
    func reloadData()
    func presentError(title: String)
    func goToDetailsScene(listing: Listing, category: Category?)
}

final class ListingPresenter {
    unowned private let view: ListingPresenterView
    private let itemsService: ItemsServiceProtocol
    private let dispatchGroup: DispatchGroup

    private(set) var displayableListings: [Listing] = []
    private(set) var categories: [Category] = []
    private var didPresentError: Bool = false
    private var listings: [Listing] = []

    init(
        view: ListingPresenterView,
        itemsService: ItemsServiceProtocol = ItemsService.shared,
        dispatchGroup: DispatchGroup = .init()
    ) {
        self.view = view
        self.itemsService = itemsService
        self.dispatchGroup = dispatchGroup
    }
}

extension ListingPresenter {
    func refresh() {
        getItems()
        getCategories()

        dispatchGroup.notify(
            queue: .main,
            execute: { [weak self] in
                guard let me = self else { return }
                me.didPresentError = false
                me.view.reloadData()
            }
        )
    }

    func didSelectRow(at indexPath: IndexPath) {
        let index = indexPath.row
        let listing = displayableListings[index]
        let category = categories.compactMap { category -> Category? in
            if category.id == listing.categoryId {
                return category
            }
            return nil
        }.first

        view.goToDetailsScene(listing: listing, category: category)
    }

    var numberOfRowsInSection: Int {
        return displayableListings.count
    }

    var numberOfRowsInComponent: Int {
        return categories.count
    }

    func titleForRow(at row: Int) -> String {
        return categories[row].name
    }

    func didSelectPickerRow(at row: Int) {
        let shouldResetFiltersByCategory = row == 0
        let selectedCategoryId = categories[row].id

        if shouldResetFiltersByCategory {
            displayableListings = listings
        } else {
            let filteredListings = listings.filter { (listing) -> Bool in
                return selectedCategoryId == listing.categoryId
            }
            displayableListings = filteredListings
        }
        view.reloadData()
    }

    func listingAndCategory(at indexPath: IndexPath) -> (Listing, Category?) {
        let index = indexPath.row
        let selectedListing = displayableListings[index]
        let selectedCategoryId = selectedListing.categoryId
        let selectedCategory = categories.compactMap { category -> Category? in
            if category.id == selectedCategoryId {
                return category
            }
            return nil
        }.first

        return (selectedListing, selectedCategory)
    }
}

private extension ListingPresenter {
    func getItems() {
        dispatchGroup.enter()

        itemsService.getListings { [weak self] (result) in
            guard let me = self else { return }

            switch result {
            case .success(let listings):
                let sortedListings = listings.sortByUrgencyAndThenDate()
                me.displayableListings = sortedListings
                me.listings = sortedListings
            case .failure:
                if !me.didPresentError {
                    me.didPresentError = true
                    me.view.presentError(title: Localizables.networkingError)
                }
            }
            me.dispatchGroup.leave()
        }
    }

    func getCategories() {
        dispatchGroup.enter()

        itemsService.getCategories { [weak self] (result) in
            guard let me = self else { return }

            switch result {
            case .success(let categories):
                me.categories = categories
                me.categories.insert(Category(id: 0, name: Localizables.defaultCategory), at: 0)
            case .failure:
                if !me.didPresentError {
                    me.didPresentError = true
                    me.view.presentError(title: Localizables.networkingError)
                }
            }
            me.dispatchGroup.leave()
        }
    }
}
