//
//  ListingViewController.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import UIKit

class ListingViewController: UIViewController {
    private let tableView = UITableView()
    private let pickerView = UIPickerView()
    var presenter: ListingPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        configureConstraints()
        presenter.refresh()
    }
}

extension ListingViewController: ListingPresenterView {
    func reloadData() {
        tableView.reloadData()
        pickerView.reloadAllComponents()
    }

    func presentError(title: String) {
        DispatchQueue.main.async {
            self.showAlert(title: title)
        }
    }

    func goToDetailsScene(listing: Listing, category: Category?) {
        let listingDetailsViewController = ListingDetailsViewController()
        let listingDetailsPresenter = ListingDetailsPresenter(
            listing: listing,
            category: category,
            view: listingDetailsViewController
        )
        listingDetailsViewController.presenter = listingDetailsPresenter
        navigationController?.pushViewController(listingDetailsViewController, animated: true)
    }
}

extension ListingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ListingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRowsInSection
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let listingCell = cell as? ListingTableViewCell else { return }

        listingCell.refreshData()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let listingCell = tableView.dequeueReusableCell(withIdentifier: "\(ListingTableViewCell.self)")as? ListingTableViewCell else { return UITableViewCell() }

        let (listing, category) = presenter.listingAndCategory(at: indexPath)
        listingCell.configure(listing: listing, category: category)
        return listingCell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

private extension ListingViewController {
    func configureView() {
        tableView.separatorStyle = .singleLine
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 0)
        tableView.register(ListingTableViewCell.self, forCellReuseIdentifier: "\(ListingTableViewCell.self)")
        tableView.delegate = self
        tableView.dataSource = self
        pickerView.dataSource = self
        pickerView.delegate = self
        title = Localizables.title
        view.backgroundColor = .white
        
        view.addSubview(pickerView)
        view.addSubview(tableView)

        removeEmptyCells()
    }

    func configureConstraints() {
        let safeArea = view.safeAreaLayoutGuide

        tableView.translatesAutoresizingMaskIntoConstraints = false
        pickerView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            pickerView.topAnchor.constraint(equalTo: safeArea.topAnchor),
            pickerView.heightAnchor.constraint(equalToConstant: 120),
            pickerView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            pickerView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),

            tableView.topAnchor.constraint(equalTo: pickerView.bottomAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor)
        ])
    }

    func removeEmptyCells() {
        tableView.tableFooterView = UIView()
    }
}


extension ListingViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return presenter.numberOfRowsInComponent
    }
}

extension ListingViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return presenter.titleForRow(at: row)
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        presenter.didSelectPickerRow(at: row)
    }
}
