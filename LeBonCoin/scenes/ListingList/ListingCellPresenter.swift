//
//  ListingCellPresenter.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import UIKit

protocol ListingCellView: AnyObject {
    func reload(image: UIImage)
    func reload(listing: Listing, category: Category?)
}

final class ListingCellPresenter {
    private let listing: Listing
    private let category: Category?
    private let imageDownloaderService: ImageDownloaderServiceProtocol
    unowned private let view: ListingCellView

    init(
        listing: Listing,
        category: Category?,
        view: ListingCellView,
        imageDownloaderService: ImageDownloaderServiceProtocol = ImageDownloaderService.shared
    ) {
        self.listing = listing
        self.category = category
        self.view = view
        self.imageDownloaderService = imageDownloaderService
    }

    func refresh() {
        view.reload(listing: listing, category: category)

        guard let thumbURL = listing.imagesURL.thumb else { return }
        imageDownloaderService.downloadImage(url: thumbURL) { [weak self] (result) in
            guard let me = self else { return }

            switch result {
            case .success(let image):
                me.view.reload(image: image)
            case .failure(let error):
                print("Error when fetching thumb listing image = \(error.localizedDescription)")
            }
        }
    }
}
