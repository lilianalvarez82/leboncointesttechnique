//
//  AppDelegate.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {

        setRootViewController()
        return true
    }
}

extension AppDelegate {
    private func setRootViewController() {
        window = UIWindow(frame: UIScreen.main.bounds)

        let listingViewController = ListingViewController()
        let presenter = ListingPresenter(view: listingViewController)
        listingViewController.presenter = presenter
        let listingNavigationViewController = UINavigationController(rootViewController: listingViewController)

        window?.rootViewController = listingNavigationViewController
        window?.makeKeyAndVisible()
    }
}
