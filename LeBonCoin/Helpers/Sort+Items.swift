//
//  Sort+Items.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import Foundation

extension Array where Element == Listing {
    func sortByUrgencyAndThenDate() -> [Listing] {
        var listings = self

        listings.sort { itemOne, itemTwo -> Bool in
            if itemOne.isUrgent && !itemTwo.isUrgent {
                return true
            } else if !itemOne.isUrgent && itemTwo.isUrgent {
                return false
            } else {
                return itemOne.creationDate > itemTwo.creationDate
            }
        }

        return listings
    }
}
