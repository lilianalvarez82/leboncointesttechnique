//
//  Formatter+Date.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import Foundation

extension Date {
    func toDisplayableString(
        locale: Locale = Locale(identifier: "fr_FR"),
        dateStyle: DateFormatter.Style = .long
    ) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.locale = locale
        dateFormatter.dateStyle = dateStyle
        return dateFormatter.string(from: self)
    }
}
