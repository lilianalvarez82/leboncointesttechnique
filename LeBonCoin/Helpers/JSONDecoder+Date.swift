//
//  JSONDecoder+Date.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import Foundation

final class DateJSONDecoder: JSONDecoder {
    init(dateFormat: String = "yyyy-MM-dd'T'HH:mm:ssZ") {
        super.init()

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateDecodingStrategy = .formatted(dateFormatter)
    }
}
