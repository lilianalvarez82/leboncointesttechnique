//
//  UIAlertViewController+UIViewController.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import UIKit

extension UIViewController {
    func showAlert(title: String?, message: String? = nil) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: Localizables.ok, style: .default, handler: nil)
        alertVC.addAction(okAction)
        present(alertVC, animated: true)
    }
}
