//
//  Localizables.swift
//  LeBonCoin
//
//  Created by Lilian on 11/02/2021.
//

import Foundation

/// Ideally, this should be in a Localizables and then called using a generated enum in order to avoid Strings inside the code.
/// Strings are not checked by the compiler : hence, it's an error prone behavior.
/// SwiftGen lib might be a solution for this.

internal enum Localizables {
    static let undefinedPrice = "indisponible"
    static let networkingError = "Une erreur est survenue."
    static let ok = "Ok"
    static let title = "Leboncoin"
    static let urgent = "URGENT !"
    static let defaultCategory = "Toutes catégories"
}

