//
//  MockItemsServiceFailure.swift
//  LeBonCoinTests
//
//  Created by Lilian on 12/02/2021.
//

import XCTest
@testable import LeBonCoin

final class MockItemsServiceFailure: ItemsServiceProtocol {
    func getListings(completion: @escaping (CompletionHandler<[Listing]>)) {
        completion(.failure(.internetConnectivity))
    }

    func getCategories(completion: @escaping (CompletionHandler<[LeBonCoin.Category]>)) {
        completion(.failure(.internetConnectivity))
    }
}
