//
//  MockImageDownloaderService.swift
//  LeBonCoinTests
//
//  Created by Lilian on 12/02/2021.
//

import UIKit
import XCTest
@testable import LeBonCoin

final class MockImageDownloaderService: ImageDownloaderServiceProtocol {
    func downloadImage(url: URL, completion: @escaping (CompletionHandler<UIImage>)) {
        guard let image = UIImage(named: "placeholder") else {
            XCTFail("MockImageDownloaderService was not configure correctly.")
            return
        }
        completion(.success(image))
    }
}
