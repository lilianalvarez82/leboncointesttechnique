//
//  MockItemsServiceSuccess.swift
//  LeBonCoinTests
//
//  Created by Lilian on 12/02/2021.
//

import XCTest
@testable import LeBonCoin

final class MockItemsServiceSuccess: ItemsServiceProtocol {
    func getListings(completion: @escaping (CompletionHandler<[Listing]>)) {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "Listings", withExtension: "json") else {
            XCTFail("Missing file: Listings.json")
            return
        }
        let dateJSONDecoder = DateJSONDecoder()
        let data = try! Data(contentsOf: url)
        let listings = try! dateJSONDecoder.decode([Listing].self, from: data)
        completion(.success(listings))
    }

    func getCategories(completion: @escaping (CompletionHandler<[LeBonCoin.Category]>)) {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "Categories", withExtension: "json") else {
            XCTFail("Missing file: Categories.json")
            return
        }
        let dateJSONDecoder = DateJSONDecoder()
        let data = try! Data(contentsOf: url)
        let categories = try! dateJSONDecoder.decode([LeBonCoin.Category].self, from: data)
        completion(.success(categories))
    }
}
