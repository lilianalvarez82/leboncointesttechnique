//
//  ListingPresenterTests.swift
//  LeBonCoinTests
//
//  Created by Lilian on 11/02/2021.
//

import XCTest
@testable import LeBonCoin

final class ListingPresenterTests: XCTestCase {
    private var presenter: ListingPresenter?
    private var mockListingPresenterView: MockListingPresenterView?
    
    override func setUpWithError() throws {
        mockListingPresenterView = MockListingPresenterView()
    }

    override func tearDownWithError() throws {
        mockListingPresenterView = nil
        presenter = nil
    }

    func testRefreshWithError() {
        // Given :
        let mockItemsService: ItemsServiceProtocol = MockItemsServiceFailure()
        presenter = ListingPresenter(view: mockListingPresenterView!, itemsService: mockItemsService)
        let exp = expectation(description: "MockListingPresenterView expectation")
        mockListingPresenterView?.testExpectation = exp

        // When :
        presenter?.refresh()
        wait(for: [exp], timeout: 1)

        // Then :
        XCTAssertEqual(mockListingPresenterView!.titleError, Localizables.networkingError)
    }

    func testRefreshDataAndGoToDetails() {
        // Given :
        let mockItemsService: ItemsServiceProtocol = MockItemsServiceSuccess()
        presenter = ListingPresenter(view: mockListingPresenterView!, itemsService: mockItemsService)
        let exp = expectation(description: "MockListingPresenterView expectation")
        mockListingPresenterView?.testExpectation = exp

        // When :
        presenter?.refresh()
        let index = IndexPath(row: 0, section: 0)
        presenter?.didSelectRow(at: index)
        wait(for: [exp], timeout: 1)

        // Then :
        guard let mockListing = mockListingPresenterView?.listing else {
            XCTFail("Missing Listing in MockListingPresenterView")
            return
        }

        XCTAssertEqual(mockListing.id, 1547408955)
        XCTAssertEqual(mockListing.categoryId, 7)
        XCTAssertEqual(mockListing.title, "Vinyle Elliott Murphy Just A Story From America")
        XCTAssertEqual(mockListing.description, "A vendre 1 vinyle de  Elliott Murphy Just A Story From America, en bon état remis en main propre. Pressage hollandais 1977, CBS – 81881")
        XCTAssertEqual(mockListing.creationDate.toDisplayableString(), "6 novembre 2019")
        XCTAssertEqual(mockListing.urgent, "URGENT !")
        XCTAssertEqual(mockListing.displayablePrice(), "10,00 €")
    }

    func testCategoryAndListingsCount() {
        // Given :
        let mockItemsService: ItemsServiceProtocol = MockItemsServiceSuccess()
        presenter = ListingPresenter(view: mockListingPresenterView!, itemsService: mockItemsService)
        let exp = expectation(description: "MockListingPresenterView expectation")
        mockListingPresenterView?.testExpectation = exp

        // When :
        presenter?.refresh()
        wait(for: [exp], timeout: 1)

        // Then :
        XCTAssertEqual(presenter?.numberOfRowsInSection, 300)
        XCTAssertEqual(presenter?.numberOfRowsInComponent, 12)
        XCTAssertEqual(presenter?.titleForRow(at: 0), Localizables.defaultCategory)
    }

    func testFilterListingByCategory() {
        // Given :
        let mockItemsService: ItemsServiceProtocol = MockItemsServiceSuccess()
        presenter = ListingPresenter(view: mockListingPresenterView!, itemsService: mockItemsService)
        let exp = expectation(description: "MockListingPresenterView expectation")
        mockListingPresenterView?.testExpectation = exp

        // When :
        presenter?.refresh()
        presenter?.didSelectPickerRow(at: 4)
        wait(for: [exp], timeout: 1)

        // Then :
        XCTAssertEqual(presenter?.displayableListings.count, 76)
        for listing in presenter!.displayableListings {
            XCTAssertEqual(listing.categoryId, 4)
        }
    }

    func testSortingListings() {
        // Given :
        let listingOne = Listing(id: 1, categoryId: 1, title: "title 1", description: "description 1", price: 10, imagesURL: ImagesURL(small: nil, thumb: nil), creationDate: Date() + 1, isUrgent: false)
        let listingTwo = Listing(id: 2, categoryId: 1, title: "title 2", description: "description 2", price: 10, imagesURL: ImagesURL(small: nil, thumb: nil), creationDate: Date() + 2, isUrgent: true)
        let listingThree = Listing(id: 3, categoryId: 1, title: "title 3", description: "description 3", price: 10, imagesURL: ImagesURL(small: nil, thumb: nil), creationDate: Date() + 3, isUrgent: false)
        let listingFour = Listing(id: 4, categoryId: 1, title: "title 4", description: "description 4", price: 10, imagesURL: ImagesURL(small: nil, thumb: nil), creationDate: Date() + 4, isUrgent: true)
        let listings = [listingOne, listingTwo, listingThree, listingFour]

        // When :
        let sortedListings = listings.sortByUrgencyAndThenDate()

        // Then :
        XCTAssertTrue(sortedListings[0].id == listingFour.id)
        XCTAssertTrue(sortedListings[1].id == listingTwo.id)
        XCTAssertTrue(sortedListings[3].id == listingOne.id)
        XCTAssertTrue(sortedListings[2].id == listingThree.id)
    }
}
