//
//  MockListingPresenterView.swift
//  LeBonCoinTests
//
//  Created by Lilian on 12/02/2021.
//

import XCTest
import UIKit
@testable import LeBonCoin

final class MockListingPresenterView: ListingPresenterView {
    var didCallReloadData = false
    var titleError: String?
    var testExpectation: XCTestExpectation?
    var listing: Listing?
    var category: LeBonCoin.Category?

    func reloadData() {
        guard let expectation = testExpectation else {
            XCTFail("MockListingPresenterView was not configure correctly.")
            return
        }

        didCallReloadData = true
        expectation.fulfill()
    }

    func presentError(title: String) {
        guard let expectation = testExpectation else {
            XCTFail("MockListingPresenterView was not configure correctly.")
            return
        }

        titleError = title
        expectation.fulfill()
    }

    func goToDetailsScene(listing: Listing, category: LeBonCoin.Category?) {
        guard let expectation = testExpectation else {
            XCTFail("MockListingPresenterView was not configure correctly.")
            return
        }

        self.listing = listing
        self.category = category
        expectation.fulfill()
    }
}
