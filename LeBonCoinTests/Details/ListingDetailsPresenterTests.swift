//
//  ListingDetailsPresenterTests.swift
//  LeBonCoinTests
//
//  Created by Lilian on 12/02/2021.
//

import XCTest
@testable import LeBonCoin

final class ListingDetailsPresenterTests: XCTestCase {
    private var presenter: ListingDetailsPresenter?
    private var mockListingDetailsPresenterView: MockListingDetailsPresenterView?
    private var mockImageDownloaderService: ImageDownloaderServiceProtocol?
    private var listing: Listing?
    private var category: LeBonCoin.Category?

    override func setUpWithError() throws {
        mockListingDetailsPresenterView = MockListingDetailsPresenterView()
        mockImageDownloaderService = MockImageDownloaderService()
    }

    override func tearDownWithError() throws {
        mockListingDetailsPresenterView = nil
        mockImageDownloaderService = nil
        presenter = nil
        listing = nil
        category = nil
    }

    func testRefresh() {
        // Given :
        fetchDataFromJsonFiles()
        presenter = ListingDetailsPresenter(
            listing: listing!,
            category: category,
            imageDownloaderService: mockImageDownloaderService!,
            view: mockListingDetailsPresenterView!
        )
        let exp = expectation(description: "MockListingPresenterView expectation")
        mockListingDetailsPresenterView?.testExpectation = exp

        // When :
        presenter?.refresh()
        wait(for: [exp], timeout: 2)

        // Then :
        guard let mockListing = mockListingDetailsPresenterView?.listing,
              let mockImage = mockListingDetailsPresenterView?.image,
              let mockCategory = mockListingDetailsPresenterView?.category else {
            XCTFail("Initialization of MockListingDetailsPresenterView failed.")
            return
        }

        XCTAssertEqual(mockListing.id, 1461267313)
        XCTAssertEqual(mockListing.categoryId, 4)
        XCTAssertEqual(mockListing.title, "Statue homme noir assis en plâtre polychrome")
        XCTAssertEqual(mockListing.description, "Magnifique Statuette homme noir assis fumant le cigare en terre cuite et plâtre polychrome réalisée à la main.  Poids  1,900 kg en très bon état, aucun éclat  !  Hauteur 18 cm  Largeur : 16 cm Profondeur : 18cm  Création Jacky SAMSON  OPTIMUM  PARIS  Possibilité de remise sur place en gare de Fontainebleau ou Paris gare de Lyon, en espèces (heure et jour du rendez-vous au choix). Envoi possible ! Si cet article est toujours visible sur le site c\'est qu\'il est encore disponible")
        XCTAssertEqual(mockListing.creationDate.toDisplayableString(), "5 novembre 2019")
        XCTAssertEqual(mockListing.urgent, nil)
        XCTAssertEqual(mockListing.displayablePrice(), "140,00 €")
        XCTAssertEqual(mockImage.accessibilityIdentifier, UIImage(named: "placeholder")!.accessibilityIdentifier)
        XCTAssertEqual(mockCategory.name, "Maison")
    }
}

extension ListingDetailsPresenterTests {
    func fetchDataFromJsonFiles() {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "Listings", withExtension: "json") else {
            XCTFail("Missing file: Listings.json")
            return
        }
        let dateJSONDecoder = DateJSONDecoder()
        let data = try! Data(contentsOf: url)
        let listings = try! dateJSONDecoder.decode([Listing].self, from: data)
        guard let listing = listings.first else {
            XCTFail("Failing decode Listing")
            return
        }
        self.listing = listing
        self.category = LeBonCoin.Category(id: 4, name: "Maison")
    }
}
