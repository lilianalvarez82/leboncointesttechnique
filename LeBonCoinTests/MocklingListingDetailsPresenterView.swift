//
//  MocklingListingDetailsPresenterView.swift
//  LeBonCoinTests
//
//  Created by Lilian on 12/02/2021.
//

import UIKit
import XCTest
@testable import LeBonCoin

final class MockListingDetailsPresenterView: ListingDetailsView {
    var testExpectation: XCTestExpectation?
    var category: LeBonCoin.Category?
    var listing: Listing?
    var image: UIImage?

    func reload(image: UIImage) {
        guard let expectation = testExpectation else {
            XCTFail("MocklingListingDetailsPresenterView was not configure correctly.")
            return
        }

        self.image = image
        expectation.fulfill()
    }

    func reload(listing: Listing, category: LeBonCoin.Category?) {
        self.listing = listing
        self.category = category
    }
}
