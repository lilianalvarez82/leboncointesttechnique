#  Test Le Bon Coin. 


## Les différents points : 

- Une architecture qui respecte le principe de responsabilité unique.
- Création des interfaces avec autolayout directement dans le code (pas de storyboard ni de
xib, ni de SwiftUI).
- Développement en Swift.
- Le code doit être versionné (Git) sur une plateforme en ligne type Github ou Bitbucket (pas de
zip) et doit être immédiatement exécutable sur la branche master.
- Aucune librairie externe n'est autorisée.
- Le projet doit être compatible pour iOS 11+.

## API : 
- ​https://raw.githubusercontent.com/leboncoin/paperclip/master/listing.json
- https://raw.githubusercontent.com/leboncoin/paperclip/master/categories.json

